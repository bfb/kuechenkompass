/** 
* 	Küchenkompass 
*	
*	recipe-servings.js
*
*/
;define(['jquery'], function ($) {
    'use strict';

    // Create the defaults once
    var pluginName = 'recipeServings', defaults = {
        singleValueAttr: null
    };
    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {
            this.$element = $(this.element);
            this.$form = $('.kk-recipe__form-calculate');
            this.$input = $('.kk-recipe__input input[type="number"]');
            this.$amounts = $('.kk-recipe__ingredients-list__amount');

            this.bindEvents();

        },

        calculateAmounts: function(e) {
            e.preventDefault();
        	var multi = parseInt(this.$input.val());
        	if (isNaN(multi) || multi < 1) {
        		multi = 4;
            }
            else if (multi > 20) {
                multi = 20;
            }

            $.each(this.$amounts, function(index, item) {
                var withNoDigits = $(item).html().match(/\D+/g);
                if (withNoDigits !== null) {
                    withNoDigits = withNoDigits[withNoDigits.length - 1];
                    if (withNoDigits === ',') {
                        withNoDigits = '';
                    }
                }
                else {
                    withNoDigits = '';
                }
                var unit = $(item).attr(this.options.singleValueAttr);

                if (unit == 0) {
                    $(item).html('');
                }
                else {
                    var newValue = unit * multi;
                    if (newValue >= 100) {
                        newValue = Math.round(newValue / 5) * 5;
                    }
                    else if (newValue >= 10) {
                        newValue = Math.round(newValue * 2) / 2;
                    }
                    else {
                        newValue = newValue.toFixed(2);
                    }

                    $(item).html((newValue).toString().replace('.', ',') + withNoDigits);
                }
            }.bind(this));
        },

        bindEvents: function () {
            this.$form.on('submit', {}, this.calculateAmounts.bind(this));
            this.$input.on('change', {}, this.calculateAmounts.bind(this));
        }
    };
    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    };
});