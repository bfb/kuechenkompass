require.config({
    paths: {
        jquery: '../bower_components/jquery/jquery',
        flexslider: 'jquery.flexslider',
        lightbox: 'lightbox-2.6.min'
    },
    shim: {
    	flexslider: {
    		deps: ['jquery']
    	},
        lightbox: {
            deps: ['jquery']
        }
    }
});



require(['jquery', 'flexslider', 'lightbox', 'recipeServings', 'gmap', 'ratingStars', 'showHide', 'comments'], function ($, flexslider, lightbox, recipeServings, gmap, ratingStars, showHide, comments) {
    'use strict';
    // use app here

    //console.log('Running jQuery %s', $().jquery);

    $('.kk-map').gmap();

    if ($('.kk-teaser__rating').length > 0) {
        $('.kk-teaser__rating').ratingStars();
    }

    /* prefixing the transition event listeners*/
    var pfx = ["webkit", "moz", "MS", "o", ""];
    function PrefixedEvent(element, type, callback, remove) {
        for (var p = 0; p < pfx.length; p++) {
            if (!pfx[p]) type = type.toLowerCase();
            if (remove) {
                element.removeEventListener(pfx[p]+type, callback, false);
            }
            else {
                element.addEventListener(pfx[p]+type, callback, false);
            }
        }
    }


    $(function() {
        /* flexslider jquery plugin, the slider on the Homepage */
        if ($('.flexslider').length) {
            
                $('.flexslider').flexslider({
                    selector        :       '.kk-slider__images > li',
                    animation       :       'slide',
                    animationSpeed  :       800,
                    pauseOnHover    :       true,
                    prevText        :       '',
                    nextText        :       '',
                    touch           :       true,
                    start           :       function() {
                        $('.kk-slider__images').addClass('active');
                    }
                });
            
        }

        if ($('.kk-teaser__commentbox form').length) {
            
                $('.kk-teaser__commentbox form').comments();
            
        }


        /*$('.kk-teaser__image, .kk-recipe__image').each(function(index, item) {
            var prop = $(item).width() / $(item).height();
            var img = $('img', item);
            if (img.length === 1) {
                $(img).css({
                    'width'     :   'auto',
                    'height'    :   'auto'
                });
                var imgProp = $(img).width() / $(img).height();
                if (imgProp > prop) {
                    $(img).addClass('rev');
                }
                $(img).removeAttr('style');
            }
        });*/


        /* click on mobile menu button opens the invisible navigation menu */
        if ($('.kk-wrapper--submenu').length == 1) {
            var subnav = true;
        }
        else {
            subnav = false;
        }

        function mobileMenu(close) {
            var $menu = $('.kk-mobile-menu');
            if ($menu.hasClass('active') || close === true) {
                $menu.removeClass('active');
                $('html').removeClass('mobile-menu-open');
                if (subnav) {
                    $('.kk-wrapper--submenu').removeClass('active');
                }
            }
            else {
                $menu.addClass('active');
                $('html').addClass('mobile-menu-open');
                if (subnav) {
                    $('.kk-wrapper--submenu').addClass('active');
                }
            }
        }

        function categories(close) {
            var $el = $('.kk-list--categories');
            if ($el.hasClass('active') || close === true) {
                $('.kk-button--mobile-categories').removeClass('active');
                $el.removeClass('active');
            }
            else {
                mobileMenu(true);
                $('.kk-button--mobile-categories').addClass('active');
                $el.addClass('active');
            }
        }

        $('a.kk-button--mobile-menu').on('click', function(e) {
            e.preventDefault();

            mobileMenu(false);
            categories(true);
        });


        /* opens the desktop search input */
        var inputField = $('.kk-search__input');
        $('.kk-search__submit').on('click', function(e) {
            if (!$(inputField).val()) {
                e.preventDefault();

                if ($(inputField).hasClass('active')) {
                    $(this).removeClass('active');
                    $(inputField).removeClass('active');
                }
                else {
                    $(inputField).focus();
                    $(this).addClass('active');
                    $(inputField).addClass('active');
                }

                mobileMenu(true);
                categories(true);
            }
        });



        var $searchbox = $('.kk-searchbox');
        var $globalSearch = $('.kk-search');
        var $input = $('.kk-search__input');
        var $inputDetail = $('.kk-searchbox__input-text:first-child');

        function mobileSearch(e) {
            e.preventDefault();

            if ($searchbox.length === 0) {
                mobileMenu(true);
                categories(true);
                $globalSearch.addClass('active animation-active');
                $input.focus();
                $('.kk-searchbox__change').css('display', 'none');
            }
            else {
                e.stopPropagation();
                e.preventDefault();

                if ($globalSearch.hasClass('active')) {
                    $('.kk-search__body').on('webkitTransitionEnd transitionend', function() {
                        $globalSearch.removeClass('animation-active');
                        $('.kk-search__body').off();
                    });
                    $globalSearch.removeClass('active');
                }
                mobileMenu(true);
                categories(true);
                $searchbox.addClass('active');
                $inputDetail.focus();
            }
        }

        function changeSearch(e) {
            if ($searchbox.hasClass('active')) {
                $searchbox.removeClass('active');
                $globalSearch.addClass('active animation-active');
                $input.focus();
            }
            else {
                $('.kk-search__body').on('webkitTransitionEnd transitionend', function() {
                    $globalSearch.removeClass('animation-active');
                    $('.kk-search__body').off();
                });
                $globalSearch.removeClass('active');
                mobileMenu(true);
                categories(true);
                $searchbox.addClass('active');
                $inputDetail.focus();
            }
        }

        $('.kk-button--mobile-search').on('click', mobileSearch);
        $('.kk-button--mobile-search').on('click', mobileSearch);
        $('.kk-wrapper--content').on('click', function() {
            $searchbox.removeClass('active');
            $('.kk-search__body').on('webkitTransitionEnd transitionend', function() {
                $globalSearch.removeClass('animation-active');
                $('.kk-search__body').off();
            });
            $globalSearch.removeClass('active');
            $input.blur();
            $inputDetail.blur();
        });
        $searchbox.on('click', function(e) {
            e.stopPropagation();
        });
        $('.kk-searchbox__change').on('click', changeSearch);






        /* opens the mobile categories */
        if ($('.kk-list--categories').length) {
            $('.kk-button--mobile-categories').on('click', function(e) {
                e.preventDefault();

                categories(false);
            });
        }
        else {
            $('.kk-button--mobile-categories').addClass('mobile-hide');
        }



        if ($('.kk-recipe__input').length == 1) {
            $('.kk-recipe').recipeServings({
                singleValueAttr: 'data-single'
            });
        }



        $('.kk-topmenu > a:first-child').on('click', function(e) {
            e.preventDefault();

            window.history.back();
        })

        if ($('.kk-js-show-hide__button').length > 0) {
            $('.kk-js-show-hide__button').showHide();
        }
    });
});