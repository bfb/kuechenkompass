/** 
*   Küchenkompass 
*   
*   Comments
*
*/
;define(['jquery', 'jquery.validate.min'], function ($) {
    'use strict';

    // Create the defaults once
    var pluginName = 'comments', defaults = {
        
    };
    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {
            var captchaName = $('#ctrl_captcha').attr('name');
            console.log(captchaName);
            this.$element = $(this.element);
            var attrs = {
                errorElement    :   'span',
                rules           :   {
                    agb         :   {
                        required    :   true
                    }
                },
                messages        :   {
                    name            :   'Bitte füllen Sie dieses Feld aus.',
                    email           :   'Bitte geben Sie eine gültige Email Addresse an.',
                    comment         :   'Dieses Feld muss ausgefüllt werden.',
                    agb             :   'Bitte stimmen Sie den AGB zu.'
                }
            };
            attrs['messages'][captchaName]   =   'Bitte tragen Sie das Ergebnis ein.';
            this.$element.validate(attrs);
        },

        bindEvents: function () {

        }
    };
    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    };
});