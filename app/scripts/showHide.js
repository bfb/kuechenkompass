/** 
* 	Küchenkompass 
*	
*	Show / Hide
*
*/
;define(['jquery'], function ($) {
    'use strict';

    // Create the defaults once
    var pluginName = 'showHide', defaults = {
        selectorShow    : '.kk-js-show-hide__show',
        selectorHide    : '.kk-js-show-hide__hide'
    };
    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {
            this.$element = $(this.element);
            this.$showElement = $(this.options.selectorShow);
            this.$hideElement = $(this.options.selectorHide);

            this.$showElement.css('display', 'none');

            // get the rating
            this.bindEvents();
        },

        showHide: function() {
            this.$hideElement.css('display', 'none');
            this.$showElement.css('display', 'block');
        },

        bindEvents: function () {
            this.$element.on('click', {}, this.showHide.bind(this));
        }
    };
    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    };
});