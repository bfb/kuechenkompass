/** 
* 	AHB 
*	
*	MAP.js
*
*/
;define("gmap", ["jquery"], function ($) {
    // Create the defaults once
    var pluginName = "gmap", defaults = {
        propertyName: "value"
    };
    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;


        window.gmapLoaded = function () {
            this.init();
        } .bind(this);
        if (typeof aussenansichtImg != 'undefined' && aussenansichtImg != null) {
            this.aussenansicht = aussenansichtImg;
        } else {
            this.aussenansicht = false;
        }

        require(["http://maps.google.com/maps/api/js?v=3&sensor=true&language=de&callback=gmapLoaded"]);
    }
    Plugin.prototype = {
        init: function () {
            this.$element = $(this.element);
            this.$showRoute_el = $(".kk-map__show-route");
            this.$showRoute_btn = $(".kk-map__calculate-address");
            this.$route_el = $(".kk-map__route");
            this.$flag_el = $('.kk-map__flag');
            this.$routeflag_el = $('.kk-map__routeflag');
			this.$progress_layer = $('.kk-map__calculation-in-progress');
            this.$map_canvas = $('.kk-map__canvas');
			this.found_start_addr_string;
            this.map;
			this.geocoder;
            this.flag;
            this.startFlag;
            this.currentPosition;
            this.directionsDisplay;
            this.directionsService;
            this.destinationLatitude = this.$map_canvas.data("lat");
            this.destinationLongitude = this.$map_canvas.data("lon");
            this.currentPositionMarker;
            this.startPositionMarker;
			this.startLat;
			this.startLon;

            this.initializeMap();
            this.bindEvents();

        },

        initializeMap: function () {

            this.currentPosition = new google.maps.LatLng(this.destinationLatitude, this.destinationLongitude);
            this.directionsDisplay = new google.maps.DirectionsRenderer();
			this.geocoder = new google.maps.Geocoder();

            this.map = new google.maps.Map(this.$map_canvas[0], {
                zoom: 15,
                center: this.currentPosition,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL,
                    position: google.maps.ControlPosition.LEFT_BOTTOM
                },
                mapTypeControl: true,
                mapTypeControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                }
            });

            this.currentPositionMarker = new google.maps.Marker({
                position: this.currentPosition,
                map: this.map,
                title: "Current position"
            });

            this.addFlag();
        },

        calculateRoute: function () {

            if (this.directionsDisplay != null) {
                this.directionsDisplay.setMap(null);
                this.directionsDisplay = null;
                this.$route_el.empty();
            }

            this.directionsDisplay = new google.maps.DirectionsRenderer();
            this.directionsService = new google.maps.DirectionsService();
            this.currentPosition = new google.maps.LatLng(this.startLat, this.startLon);
            this.directionsDisplay.setMap(this.map);

            var targetDestination = new google.maps.LatLng(this.destinationLatitude, this.destinationLongitude);

			if(this.startPositionMarker)
				this.startPositionMarker.setMap(null);

            this.startPositionMarker = new google.maps.Marker({
                position: this.currentPosition,
                map: this.map,
                title: "Start position"
            });

            if (this.currentPosition != '' && this.targetDestination != '') {

                var request = {
                    origin: this.currentPosition,
                    destination: targetDestination,
                    travelMode: google.maps.DirectionsTravelMode["DRIVING"]
                };

                this.directionsService.route(request, function (response, status) {

                    if (status == google.maps.DirectionsStatus.OK) {
                        this.flag.close();
                        this.endCalculation();
                        this.directionsDisplay.setPanel(this.$route_el.get()[0]);
                        this.directionsDisplay.setDirections(response);
                        this.$route_el.addClass("show");

						this.setStartAddressStringByLatLon();
						
						// deprecated
                        // this.addStartFlag();
                    }
                }.bind(this));

            }
        },

        addFlag: function () {

            this.flag = new google.maps.InfoWindow({ disableAutoPan: true });
            this.flag.setContent(this.$flag_el.get()[0]);
            this.$flag_el.css({ top: 0, height: '115px' });
            this.$flag_el.fadeIn();

            if (this.aussenansicht) {
                this.flag.open(this.map, this.currentPositionMarker);
                this.map.panBy(44, -150);
            }

            google.maps.event.addListener(this.currentPositionMarker, 'click', function () {
                this.flag.open(this.map, this.currentPositionMarker);
            }.bind(this));

        },

        addStartFlag: function () {
            //adressfeld zur korrektur der startadresse
			
			if(this.startFlag)
				this.startFlag.close();

            var stLat = this.currentPosition.jb, stLon = this.currentPosition.kb;
            this.startFlag = new google.maps.InfoWindow({ disableAutoPan: false });
            this.startFlag.setContent(this.$routeflag_el.get()[0]);
            this.startFlag.open(this.map, this.startPositionMarker);
            this.$routeflag_el.css({ position: "inherit", top: 0 });

            $('#right_address').submit(function (evt) {
                evt.preventDefault();
                var rightAddress = document.getElementById('raddr').value;
                if (rightAddress == 'Ihre Adresse') {
                    alert('Bitte geben sie Ihre Adresse in das Suchfeld ein.');
                } else {

					this.geocoder.geocode(
						{
							address: rightAddress
						},

						function(result, status) {
							if (status == google.maps.GeocoderStatus.OK) {
		
								this.found_start_addr_string = result[0].formatted_address;
								this.startLat = result[0].geometry.location.d;
								this.startLon = result[0].geometry.location.e;
		
								this.calculateRoute();
							}
						}.bind(this)
						
					);

                }
                return false;
            }.bind(this));
        },
		
        locSuccess: function (position) {
		   this.startLat = position.coords.latitude;
		   this.startLon = position.coords.longitude;

		   this.calculateRoute();
        },

        locError: function (error) {
            this.endCalculation();
            
			alert("Ihre Position konnte nicht ermittelt werden.");
        },

		setStartAddressStringByLatLon: function() {

			this.geocoder.geocode({'latLng': this.currentPosition}, function(results, status) {
				this.$showRoute_el.find('#Startaddress').val(results[0].formatted_address);
			}.bind(this));

		},

        getStartAddressByGeoloc: function() {
            this.startCalculation();

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(this.locSuccess.bind(this), this.locError.bind(this), {timeout: 5000});
            }
        },

		getStartAddressByUserString: function(event) {
            event.preventDefault();

			var string = $(event.target).find("#Startaddress").val();

            if (string.length === 0) {
                this.getStartAddressByGeoloc();
            }
            else {
    			this.geocoder.geocode(
    				{
    					address: string
    				},
    				function(result, status) {
    					if (status == google.maps.GeocoderStatus.OK) {

                            this.found_start_addr_string = result[0].formatted_address;
    						this.startLat = result[0].geometry.location.lat();
    						this.startLon = result[0].geometry.location.lng();

    						this.calculateRoute();
    					}
    				}.bind(this)
    			);
            }
		},

        startCalculation: function () {
			this.$progress_layer.addClass("show");
            this.$showRoute_el.addClass("process");
        },

        endCalculation: function () {
			this.$progress_layer.removeClass("show");
            this.$showRoute_el.removeClass("process");
        },

        bindEvents: function () {
            this.$showRoute_btn.on("click", {}, this.getStartAddressByGeoloc.bind(this));
			this.$showRoute_el.on("submit", {}, this.getStartAddressByUserString.bind(this));
        }
    };
 
    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    };
});
