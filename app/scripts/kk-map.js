define(['jquery', 'async!gmaps'], function() {
	var	defaults = {
			destination 	: 	'germany',
            desTitle		: 	'Deutschland',
            elMap			: 	$('#map'),
            elRoute			: 	$('#route'),
            $elCheckbox		: 	$('input[type="checkbox"]'),
            $elInput		: 	$('input[type="text"]'),
            $elForm			: 	$('form'),
            zoom			: 	16,
            markerAnimation	: 	google.maps.Animation.DROP
        };
	var geocoder,
		destination,
		start,
		map,
		directionsService,
		directionsDisplay,
		errorCount;

	return function(options) {

		this.options = $.extend( {}, defaults, options );
		var that_ = this;
		
		this.initialize = function() {
			errorCount = 0;
			geocoder = new google.maps.Geocoder();
			geocoder.geocode(
				{
					address: 	this.options.destination
				},
				function(result, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						destination = new google.maps.LatLng(result[0].geometry.location.d, result[0].geometry.location.e);
						startMap();
					}
				}
			);
		};

		this.startMap = function() {
			var mapOptions = {
			  center: destination,
			  zoom: this.options.zoom,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			map = new google.maps.Map(options.elMap.get()[0], mapOptions);

			var marker = new google.maps.Marker({
				position: destination,
				map: map,
				title: this.options.desTitle,
				animation: this.options.markerAnimation
			});

			directionsService = new google.maps.DirectionsService();
			directionsDisplay = new google.maps.DirectionsRenderer();
		}

		this.geoPositionError = function(error) {
			if (error.code == 1) {
				errorCount++;
				if (errorCount > 1) {
					alert("Nutzung der Standortabfrage wurde verweigert, um diese zu nutzen erlauben Sie der Seite in den Seiteneinstellungen Ihres Browsers bitte den Standort abfragen zu dürfen.\nOder nutzen Sie einfach das Eingabefeld unter der Karte.");
				}
			}
			$('.kk-checkbox-input').prop('checked', false);
		}

		this.getRoute = function() {
			directionsDisplay.setMap(null);
			directionsDisplay.setPanel(null);
			directionsDisplay.setMap(map);
			directionsDisplay.setPanel(that_.options.elRoute.get()[0]);

			var address = $('#address-start').val();
			geocoder.geocode(
				{
					address: 	address
				},
				function(result, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						start = new google.maps.LatLng(result[0].geometry.location.d, result[0].geometry.location.e);
						startRoute();
					}
				}
			);
		}

		this.startRoute = function() {
			var request = {
	            origin: start,
	            destination: destination,
	            travelMode: google.maps.DirectionsTravelMode["DRIVING"]
	        };

	        directionsService.route(request, function (response, status) {
	        	directionsDisplay.setDirections(response);
	        });
	    }

		this.options.$elCheckbox.on('change', function() {
			if ($(this).prop('checked') == true) {
				navigator.geolocation.getCurrentPosition(function(position) {
					geocoder.geocode(
						{
							location: 	new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
						},
						function(result, status) {
							if (status == google.maps.GeocoderStatus.OK) {
								this.options.$elInput.val(result[0].formatted_address);
								that_.getRoute();
							}
						}
					);
				}, that_.geoPositionError, {timeout: 6000});
			}
		});

		this.options.$elForm.on('submit', function(e) {
			e.preventDefault();
			that_.getRoute();
		});



		this.initialize();

	}
});