/*global define */
define([], function () {

    /* prefixing the transition event listeners*/
    var pfx = ["webkit", "moz", "MS", "o", ""];
    function PrefixedEvent(element, type, callback, remove) {
        for (var p = 0; p < pfx.length; p++) {
            if (!pfx[p]) type = type.toLowerCase();
            if (remove) {
                element.removeEventListener(pfx[p]+type, callback, false);
            }
            else {
                element.addEventListener(pfx[p]+type, callback, false);
            }
        }
    }


    $(function() {
    	/* flexslider jquery plugin, the slider on the Homepage */
	    if ($('.flexslider').length == 1) {
	        require(['flexslider'], function (flexslider) {
                $('.flexslider').css('visibility', 'visible');
	            $('.flexslider').flexslider({
	                selector        :       '.kk-slider__images > li',
	                animation       :       'slide',
	                animationSpeed  :       800,
	                pauseOnHover    :       true,
	                prevText        :       '',
	                nextText        :       ''
	            });
	        });
	    }


	    /* click on mobile menu button opens the invisible navigation menu */
        if ($('.kk-wrapper--submenu').length == 1) {
            var subnav = true;
        }
        else {
            subnav = false;
        }

        $('a.kk-button--mobile-menu').on('click', function(e) {
            e.preventDefault();

            if ($('.kk-wrapper--content').hasClass('active')) {
                $('.kk-wrapper--content').removeClass('active');
                if (subnav) {
                    $('.kk-wrapper--submenu').removeClass('active');
                }
            }
            else {
                $('.kk-wrapper--content').addClass('active');
                if (subnav) {
                    $('.kk-wrapper--submenu').addClass('active');
                }
            }
        });


        /* opens the desktop search input */
        var inputField = $('.kk-search__input');
        $('.kk-search__submit').on('click', function(e) {
        	if (!$(inputField).val()) {
	        	e.preventDefault();

	        	if ($(inputField).hasClass('active')) {
	        		$(this).removeClass('active');
	        		$(inputField).removeClass('active');
	        	}
	        	else {
	        		$(this).addClass('active');
	        		$(inputField).addClass('active');
	        	}
	        }
        });

        /* opens the mobile search input */
        function openSearch(button, box, transitionElement) {
            $(button).on('click', function(e) {
                e.preventDefault();

                if ($(box).hasClass('active')) {
                    $(box).removeClass('active');
                    PrefixedEvent(document.getElementsByClassName(transitionElement)[0], 'transitionEnd', function() {
                        $(box).removeClass('animation-active');
                        PrefixedEvent(document.getElementsByClassName(transitionElement)[0], 'transitionEnd', arguments.callee, true);
                    });
                }
                else {
                    $(box).addClass('active animation-active');
                }
            })
        }

        openSearch('.kk-button--mobile-search', '.kk-search', 'kk-search__body');


        /* opens the mobile recipe search */
        var button = $('.kk-searchbox__input-submit');
        var box = $('.kk-searchbox');
        var input = $('.kk-searchbox__input-text');

        function searchRecipes(e) {
            e.preventDefault();

            box.addClass('active');
            input.focus();
            button.off('click');
        }

        button.on('click', searchRecipes);
        input.on('blur', function() {
            box.removeClass('active');
            button.on('click', searchRecipes);
        })



        /* opens the mobile categories */
        var categories = $('.kk-list--categories');
        $('.kk-button--mobile-categories').on('click', function(e) {
            e.preventDefault();

            if ($(categories).hasClass('active')) {
                $(this).removeClass('active');
                $(categories).removeClass('active');
            }
            else {
                $(this).addClass('active');
                $(categories).addClass('active');
            }
        });
	});


});