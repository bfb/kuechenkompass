/** 
* 	Küchenkompass 
*	
*	Rating Stars
*
*/
;define(['jquery'], function ($) {
    'use strict';

    // Create the defaults once
    var pluginName = 'ratingStars', defaults = {
        selectorStars   : 'ol > li',
        selectorText    : '.kk-teaser__rating__text'
    };
    // The actual plugin constructor
    function Plugin(element, options) {
        this.element = element;
        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {
        init: function () {
            this.$element = $(this.element);
            this.$stars = this.$element.find('ol > li');
            this.$text = this.$element.find('.kk-teaser__rating__text');
            this.$text.html('&nbsp;');
            this.url = this.$element.attr('data-url');

            // get the rating
            this.$stars.removeClass('hover unhover inactive');
            var avg = Math.round(parseFloat(this.$element.attr('data-avg')));
            
            for (var i = 5; i >= avg + 1; i--) {
                $(this.$stars[i - 1]).addClass('inactive');
            }
            this.$element.addClass('active');

            if ($('.kk-teaser__commentbox').length === 1) {
                this.bindEvents();
            }
        },

        hoverStars: function(event) {
            $(event.target).addClass('hover');
            $(event.target).prevAll().removeClass('unhover');
            $(event.target).prevAll().addClass('hover');
            $(event.target).nextAll().removeClass('hover');
            $(event.target).nextAll().addClass('unhover');
        },

        unhoverStars: function(event) {
            this.$stars.removeClass('hover unhover');
        },

        ratingClick: function(event) {
            var data = {
                ID          :       this.$element.attr('data-id'),
                value       :       this.$stars.index($(event.target)) + 1
            };

            $.ajax({
                url         :       this.url,
                method      :       'POST',
                dataType    :       'json',
                data        :       data,
                beforeSend  :       function() {
                    this.$text.html('Ihre Stimme wird übermittelt');
                }.bind(this),
                success     :       function(data) {
                    this.$text.html('Ihre Stimme wurde gespeichert');
                    this.$element.attr('data-voted', 'true');
                    this.$element.attr('data-avg', data.AVG);
                    if ($('a > .kk-teaser__rating').length === 1) {
                        $('a > .kk-teaser__rating').attr('data-avg', data.AVG);
                        $('a > .kk-teaser__rating').trigger('changeValue');
                    }
                    if ($('.kk-teaser__rating__counter').length === 1) {
                        $('.kk-teaser__rating__counter').html('(' + data.votes + ' Bewertungen)');
                    }
                    this.init();
                }.bind(this),
                error       :       function() {
                    this.$text.html('Ihre Stimme konnte nicht gespeichert werden');
                }.bind(this)
            });
        },

        hasVoted: function(event) {
            if (event.type === 'mouseenter') {
                this.$text.html('Sie haben Ihre Stimme schon abgegeben');
            }
            else {
                this.$text.html('&nbsp;');
            }
        },

        bindEvents: function () {
            this.$element.off();
            this.$stars.off();

            if (this.$element.attr('data-voted') === 'false') {
                this.$stars.on('mouseenter', {}, this.hoverStars.bind(this));
                this.$element.on('mouseleave', {}, this.unhoverStars.bind(this));

                this.$stars.on('click', {}, this.ratingClick.bind(this));
            }
            else {
                this.$element.on('mouseenter', {}, this.hasVoted.bind(this));
                this.$element.on('mouseleave', {}, this.hasVoted.bind(this));
            }
            this.$element.on('changeValue', {}, this.init.bind(this));
        }
    };
    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    };
});